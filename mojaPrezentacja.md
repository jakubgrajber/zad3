---
author: Jakub Grajber
title: Mój ulubiony zespół muzyczny
subtitle: Metallica
date: 07.11.2021
theme: Warsaw
output: beamer_presentation
header-includes: 
    \usepackage{xcolor}
    \usepackage{listings}
---

## Skład zespołu na przestrzeni lat

1. Lata 1981-1982:
* James Hetfield - gitara rytmczna, wokal;
* Lars Ulrich - perkusja;
* Ron McGovney - gitara basowa.

2. Lata 1982-1983:
* James Hetfield - gitara rytmczna, wokal;
* Lars Ulrich - perkusja;
* Cliff Burton - gitara basowa;
* Dave Mustaine - gitara wiodąca.

3. Lata 1983-1986:
* James Hetfield - gitara rytmczna, wokal;
* Lars Ulrich - perkusja;
* Cliff Burton - gitara basowa;
* Kirk Hammett - gitara wiodąca.

##

4. Lata 1986-2001:

* James Hetfield - gitara rytmczna, wokal;
* Lars Ulrich - perkusja;
* Jason Newsted - gitara basowa;
* Kirk Hammett - gitara wiodąca.

5. Lata 2001-2003:

* James Hetfield - gitara rytmczna, wokal;
* Lars Ulrich - perkusja;
* Bob Rock - gitara basowa;
* Kirk Hammett - gitara wiodąca.

6. Lata 2003-2021:

* James Hetfield - gitara rytmczna, wokal;
* Lars Ulrich - perkusja;
* Robert Trujillo - gitara basowa;
* Kirk Hammett - gitara wiodąca.

## Historia założenia zespołu

\begin{alertblock}{Spotkanie Hetfielda i Larsa}
Początki zespołu sięgają maja 1981, gdy wokalista i gitarzysta James Hetfield oraz perkusista Lars Ulrich spotkali się na jam session po zamieszczeniu ogłoszeń w tym samym wydaniu gazety The Recycler.
\end{alertblock}

\begin{block}{Aletrnatywna historia}
Inne źródła podają, że Hetfield odpowiedział na ogłoszenie zamieszczone przez Larsa w wydaniu The Recycler z początku maja 1981 – anons pokazał mu Hugh Tanner, z którym Hetfield grał w zespole Phantom Lord (później Learher Charm).
\end{block}

## Naważniejsze dokonania

\begin{block}{Wielka Czwórka}
Metallica jest zaliczana do „Wielkiej Czwórki Thrashu” razem z innymi zespołami założonymi na początku lat 80.: Slayerem, Anthraxem i Megadeth. Zespołowi przypisuje się poszerzenie granic thrashu oraz nowatorskie podejście.
\end{block}

\begin{alertblock}{Rankingi}
Metallica została sklasyfikowana na 3. pozycji listy na Najlepsze Zespoły
Metalowe Wszech Czasów sporządzonej w 2006 roku przez MTV oraz na
5. pozycji listy na 100 Najlepszych Artystów Hard Rocka sporządzonej w
2000 r. przez VH1. James Hetfield i Kirk Hammett trafili na 2. pozycję
listy z 2004 r. na 100 Najlepszych Gitarzystów Heavymetalowych Wszech
Czasów magazynu Guitar World
\end{alertblock}

## Dyskografia

![&nbsp; Kill 'Em All](kill-em-all.jpg){height=70% width=70%}

\centerline{1983}

## 

![&nbsp; Ride the Lightning](ride.png){height=70% width=70%}

\centerline{1984}

## 

![&nbsp; Master of Puppets](master-of-puppets.jpg){height=70% width=70%}

\centerline{1986}

## 

![&nbsp; ...And Justice for All](and-justice-for-all.jpg){height=70% width=70%}

\centerline{1988}

## 

![&nbsp; Metallica](black.jpg){height=70% width=70%}

\centerline{1991}

## 

![&nbsp; Load](load.jpg){height=70% width=70%}

\centerline{1996}

## 

![&nbsp; Load](reload.jpg){height=70% width=70%}

\centerline{1997}

## 

![&nbsp; St. Anger](st-anger.jpg){height=70% width=70%}

\centerline{2003}

## 

![&nbsp; Death Magnetic](death-magnetic.jpg){height=70% width=70%}

\centerline{2008}

## 

![&nbsp; Hardwired...To Self-Destruct](hardwired.jpg){height=70% width=70%}

\centerline{2016}

## Dziękuję za uwagę!

![](meta.jpg){height=100% width=100%}






